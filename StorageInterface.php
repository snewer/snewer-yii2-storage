<?php

namespace snewer\yii2storage;

interface StorageInterface {

    public function upload($binary, $extension);
    public function getUrl($path);

}