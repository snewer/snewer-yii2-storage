<?php

namespace snewer\yii2storage;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Component;
use yii\httpclient\Client;
use yii\web\HttpException;
use yii\web\ForbiddenHttpException;

class SelectelDriver extends Component implements StorageInterface {


    public $user;
    public $key;
    public $container;
    public $url;
    public $depth = 2;

    private $auth_data;

    private function getAuth()
    {
        $cache_key = 'common.components.' . __METHOD__ . '.init';
        $auth_data = Yii::$app->cache->get($cache_key);
        if($auth_data){
            return $auth_data;
        } else {

            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('get')
                ->setUrl('https://auth.selcdn.ru/')
                ->setHeaders([
                    'X-Auth-User' => $this->user,
                    'X-Auth-Key' => $this->key
                ])
                ->send();

            if(!$response->getIsOk())
                throw new HttpException($response->getStatusCode());

            if( $response->getStatusCode() == 204 ){
                $response_headers = $response->getHeaders();
                $auth_data = [];
                $auth_data['storage_url'] = $response_headers['X-Storage-Url'];
                $auth_data['auth_token'] = $response_headers['X-Auth-Token'];
                $auth_data['expire_auth_token'] = $response_headers['X-Expire-Auth-Token'];

                Yii::$app->cache->set($cache_key, $auth_data, min((int) $auth_data['expire_auth_token'], 36000));
                return $auth_data;

            } else {
                throw new ForbiddenHttpException;
            }
        }
    }

    public function init()
    {
        parent::init();
        if(!isset($this->user, $this->key))
            throw new InvalidConfigException;
        if(!isset($this->container))
            throw new InvalidConfigException;
    }

    public function getUrl($path){
        return rtrim($this->url, '/') . $path;
    }


    public function upload($source, $extension)
    {
        $client = new Client();

        $path = '/' . $this->container;
        // используем древовидную структуру директорий,
        // что бы в одной директории не накапливалось большое кол-во файлов
        for($i = 0; $i < $this->depth; $i++){
            $path .= '/' . substr(md5(microtime()), 0, 3);
        }
        $path .= '/' . uniqid() . '.' . strtolower($extension);

        $info = finfo_open(FILEINFO_MIME_TYPE);
        $mime_type = finfo_buffer($info, $source);
        finfo_close($info);

        $response = $client->createRequest()
            ->setMethod('put')
            ->setUrl(rtrim($this->auth['storage_url'], '/') . $path)
            ->setHeaders([
                'X-Auth-Token' => $this->auth['auth_token'],
                'Content-Type' => $mime_type,
                'ETag' => md5($source)
            ])
            ->setContent( $source )
            ->send();


        if(!$response->getIsOk())
            throw new HttpException($response->getStatusCode());

        return $response->getStatusCode() === '201' ? $path : false;

    }

    function deleteFile($file_path){
        // todo: реализовать метод
    }

}