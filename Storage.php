<?php

namespace snewer\yii2storage;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class Storage extends Component {

    public $drivers;

    function getStorage($id){
        if(!isset($this->drivers[$id]))
            throw new InvalidConfigException("Storage driver '$id' not found");

        return Yii::createObject($this->drivers[$id]);
    }

}