<?php

namespace snewer\yii2storage;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Component;

class FileSystemDriver extends Component implements StorageInterface {

    // путь дирректории для загрузок
    public $uploadPath;
    // URL до директории для загрузок
    public $uploadUrl;
    public $depth = 2;

    public function init()
    {
        if(!isset($this->uploadPath, $this->uploadUrl))
            throw new InvalidConfigException;
    }

    public function getUrl($path){
        return  Yii::getAlias(rtrim($this->uploadUrl, '/') . $path);
    }

    public function upload($source, $extension){
        $uploadPath = Yii::getAlias($this->uploadPath);
        $uploadPath = rtrim($uploadPath, '/');

        $path = '';
        // используем древовидную структуру директорий,
        // что бы в одной директории не накапливалось большое кол-во файлов
        for($i = 0; $i < $this->depth; $i++){
            $path .= '/' . substr(md5(microtime()), 0, 3);
            if(!is_dir($uploadPath . $path))
                mkdir($uploadPath . $path);
        }
        $path .= '/' . uniqid() . '.' . strtolower($extension);

        if(is_file($uploadPath . $path) ? false : file_put_contents($uploadPath . $path, $source)){
            return $path;
        } else {
            return false;
        }
    }

}